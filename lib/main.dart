import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {

      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // final isLandscape = MediaQuery.of(context).orientation == Orientation.landscape;
    var customSize = MediaQuery.of(context).size;
    var appBar = AppBar(

      title: Text(widget.title),
    );
    var appBarSize = appBar.preferredSize;

    print('Orientation  is ${MediaQuery.of(context).orientation}');

    return Scaffold(
      appBar: appBar,
      body: Center(
        child: Container(
          height: (customSize.height - appBarSize.height) * 0.75,
          width: customSize.width * 0.75,
          color: Colors.red,




        ),
      ),


    );
  }
}
